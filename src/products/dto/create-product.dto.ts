import { IsNotEmpty, MinLength, IsInt, Max, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  productName: string;

  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @Max(100)
  price: number;
}
