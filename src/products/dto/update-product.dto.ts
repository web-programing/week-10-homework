import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';
//update-product.dto คือ จะมีตัวใดตัวหนึ่งใน create-product-dto ก็ได้ สามารถอัพเดทได้
export class UpdateProductDto extends PartialType(CreateProductDto) {}
