import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, productName: 'ข้าวผัดหมู', price: 40 },
  { id: 2, productName: 'ข้าวมันไก่ต้ม', price: 40 },
  { id: 3, productName: 'ก๊วยเตี๋ยวไก่มะระ', price: 45 },
];
let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    console.log({ ...createProductDto });
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto, // productName, price
    };

    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      //function callback - วิ่งไปทีละ productแต่ละตัว
      return product.id === id;
    });
    if (index < 0) {
      //โยน object notfoundexception ออกไป
      throw new NotFoundException(); //เป็น object ต้องใช้ new
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('product' + JSON.stringify(products[index]));
    // console.log('update' + JSON.stringify(updateProductDto));
    const updateProduct: Product = {
      ...products[index], //เมื่อทำการ spread จะได้ id. name, price
      ...updateProductDto, //ทับของเดิมที่ต้องการอัพเดท เช่นต้งการอัพเดทราคา ก็จะอัพเดทแค่ราคา
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      //function callback - วิ่งไปทีละ productแต่ละตัว
      return product.id === id;
    });
    if (index < 0) {
      //โยน object notfoundexception ออกไป
      throw new NotFoundException(); //เป็น object ต้องใช้ new
    }
    const deleteProduct = products[index];
    products.splice(index, 1); //ลบข้อมูลตำแหน่งที่ index, จำนวน 1 ตัว
    return deleteProduct;
  }

  reset() {
    products = [
      { id: 1, productName: 'ข้าวผัดหมู', price: 40 },
      { id: 2, productName: 'ข้าวมันไก่ต้ม', price: 40 },
      { id: 3, productName: 'ก๊วยเตี๋ยวไก่มะระ', price: 45 },
    ];
    lastProductId = 4;
    return 'Reset';
  }
}
